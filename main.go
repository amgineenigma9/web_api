package main

import (
	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "root"
	dbname   = "cbs"
)

// @title  web_api
//@Version   1.4.1
// @Description to store and fetch data from server
// @Tags web_api

//@license.name asarfi
//@license.url asarfi.live

// @Success      200 {string} ok
// @BasePath     /
//@host          localhost:9090
func main() {

	httpRequest()

}
