package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	_ "webapi2/docs"

	_ "github.com/lib/pq"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

type message struct {
	Message string `json:"message"`
}

type profile struct {
	Id          int    `json:"id"`
	Name        string `json:"name"`
	Age         int    `json:"age"`
	Address     string `json:"address"`
	Email       string `json:"email"`
	PhoneNumber int    `json:"phonenumber"`
	Hobby       string `json:"hobby"`
}

// All http request is handled under http function
func httpRequest() {
	router := gin.Default()
	router.Use(cors.Default())
	router.GET("/interns", checkStatus)
	router.POST("/interns", postProfile)
	router.GET("/interns/name/:name", searcProfilebyName)
	router.GET("/interns/id/:id", searcProfilebyId)
	router.POST("/interns/delete/:id", deleteDataById)
	router.GET("/interns/details", getAllDetails)
	router.GET("/docs/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	router.Run("localhost:9090")

}

func checkStatus(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, "Ok")
}

// Add data to data base
// @Summary      stores data to data base given as input
//@id            post-profile
// @Description  store data of a person on data base
// @Tags         postProfile
// @Param        data body profile true "data"
// @Success      201  {object} profile
//@failure       400 {object} message
// @Router       /interns [post]
func postProfile(c *gin.Context) {
	var newProfile profile
	if err := c.BindJSON(&newProfile); err != nil {
		r := message{"Could not add the given data"}
		c.JSON(http.StatusNotFound, r)
		return
	}
	insertData(newProfile)
	c.IndentedJSON(http.StatusCreated, newProfile)
}

// Search profile by name
// @Summary      Show data of a person by name
//@id            search-profile-by-name
// @Description  get profile data by name
// @Tags         searchProfilebyName
// @Param        name path string true "name"
// @Success      200  {object} profile
//@failure       400 {object} profile
// @Router       /interns/name/{name} [get]
func searcProfilebyName(c *gin.Context) {
	db := getConnection()
	defer db.Close()
	name := c.Param("name") //parameter sent by user
	var rowData profile     // to store data for single user
	sqlStatements := `SELECT id, name, age, address, email,phoneNumber, hobby
	                    FROM interns
	 				   WHERE name=$1;` // Query string
	data := db.QueryRow(sqlStatements, name) // Executing Query

	if err := data.Scan(&rowData.Id, &rowData.Name, &rowData.Age, &rowData.Address,
		&rowData.Email, &rowData.PhoneNumber, &rowData.Hobby); err != nil {
		panic(err)
	} else if err == nil {
		c.IndentedJSON(http.StatusOK, rowData)
		return

	} else {
		c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Not found"})

	}
}

//Search         profile by Id
// @Summary      Show data of a person by person id
// @Description  get profile id of a person from database
//@id            search-profile-by-id
// @Tags         searchProfilebyId
// @Param        id path int true "id"
// @Success      200  {object} profile
//@faiure        400 {object} profile
// @Router       /interns/id/{id} [get]
func searcProfilebyId(c *gin.Context) {
	db := getConnection()
	defer db.Close()
	idString := c.Param("id")
	idInt, err := strconv.Atoi(idString)
	if err != nil {
		panic(err)
	}
	var rowData profile
	sqlStatements := `SELECT id, name, age, address, email,phoneNumber, hobby
	                  FROM interns WHERE id=$1 `
	fmt.Println("Statement :", sqlStatements)
	data := db.QueryRow(sqlStatements, idInt)
	if err := data.Scan(&rowData.Id, &rowData.Name, &rowData.Age, &rowData.Address,
		&rowData.Email, &rowData.PhoneNumber, &rowData.Hobby); err != nil {
		r := message{"The data with the Id you are searching could not be located at dataBase"}
		c.IndentedJSON(http.StatusNotFound, r)
		panic(err)
	} else {
		c.IndentedJSON(http.StatusOK, rowData)
		return

	}
}

// get details of all interns
//@description    The function get details accepts no data but responds with details of interns in json format
//@summary        This returns details of all interns in json format
//@tags           getAllDetails
//@id             get-all-details
//@Success         200 {object} []profile
//@Failure        401 {object} message
//@Router         /interns/details [get]
func getAllDetails(c *gin.Context) {
	// fetching all data from dataBase and sending it to server
	db := getConnection()
	fmt.Println("<<<<<<<<<<<<<<<<<<<<<")
	defer fmt.Println("Hello there", db)
	fmt.Println("<<<<<<<<<<<<<<<<<<<<<<")
	defer db.Close()
	fmt.Println("<<<<<<<<<<<<<<<<<<<<<<")
	defer fmt.Println("Hello there")
	fmt.Println("<<<<<<<<<<<<<<<<<<<<<<<")
	sqlStatements := `SELECT * FROM interns`
	rows, err := db.Query(sqlStatements)
	if err != nil {
		panic(err)
	}
	details := make([]profile, 0)
	for rows.Next() {
		var newProfile profile
		if err := rows.Scan(&newProfile.Id, &newProfile.Age, &newProfile.Name, &newProfile.Address, &newProfile.Email,
			&newProfile.PhoneNumber, &newProfile.Hobby); err != nil {
			r := message{"Could not fetched data"}
			c.JSON(http.StatusNotFound, r)
			log.Fatal(err)
		}
		details = append(details, newProfile)

	}
	c.JSON(http.StatusOK, details)
	db.Close()
}

// To delete details of a paticualar person
//@summary        It is used to delete the details of a person given by ID
//@description    It accepts the id of a particular person and deletes the recoed from data base
//@id             delete-data-by-id
//@tags           deleteDataById
//@Param          id path int true "id"
//@Success        200 {object} message
//@Failure        401 {object} message
//@Router         /interns/delete/{id} [post]
func deleteDataById(c *gin.Context) {
	//cnnecting to postgress
	db := getConnection()
	defer db.Close()
	// deleting data if id matches
	idString := c.Param("id")
	idInt, err := strconv.Atoi(idString)
	fmt.Println("ID is ", idInt)
	if err != nil {
		panic(err)
	}
	sqlStatement := `
            DELETE FROM interns
            WHERE id = $1;`
	rest, err := db.Exec(sqlStatement, idInt)
	if err != nil {
		panic(err)
	}
	rowAffected, _ := rest.RowsAffected()
	if rowAffected != 0 {
		r := message{"Successfully Deleted"}
		c.JSON(http.StatusOK, r)
		return

	} else {
		r := message{"Couldnot find the Data with corresponding Id"}
		c.JSON(http.StatusNotFound, r)
	}
}
func insertData(newProfile profile) {
	db := getConnection()
	defer db.Close()
	sqlStatements := `INSERT INTO interns ( id, name, age, address, email, phonenumber, hobby)
		VALUES ($1, $2,$3 , $4, $5, $6, $7)`
	rest, err := db.Exec(sqlStatements, newProfile.Id, newProfile.Name, newProfile.Age, newProfile.Address,
		newProfile.Email, newProfile.PhoneNumber, newProfile.Hobby)
	if err != nil {
		panic(err)
	}
	fmt.Print(rest)

}
func getConnection() *sql.DB {
	pasqlInfo := fmt.Sprintf(`host=%s port =%d dbname=%s user=%s password=%s sslmode=disable`, host,
		port, dbname, user, password)

	db, err := sql.Open("postgres", pasqlInfo)
	if err != nil {
		panic(err)
	}
	if err = db.Ping(); err != nil {
		panic(err)
	}
	return db
}
